// implementasi dari class turunan 'readystock' yang menge-extend class kue
class readystock extends kue{
    private double Jumlah;
    private double Berat;

    // pemanggilan konstruktor dari induk 'kue' 
    // untuk menginisialisasi variabel name,price, dan jumlah
    public readystock(String name, double price, int jumlah) {
        super(name, price);
        this.Jumlah = jumlah;
    }
    // mengembalikan nilai dari atribut jumlah
    public double Jumlah(){
        return this.Jumlah;
    }
    // mengembalikan nilai dari atribut berat
    public double Berat(){
        return Berat;
    }

    // menghitung total harga dari kue dengan mengalikan 
    // harga satuan kue dengan jumlah kue yang tersedia dan dengan faktor 2
    public double hitungHarga(){
        return getPrice() * Jumlah() * 2;
    }

}
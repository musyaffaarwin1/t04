
    // mendefiniskan sebuah class abstrak dengan nama kue
public abstract class kue{
    // deklarasi variable
    private String name;
    private double price;
    // konstruktor class kue dgn parameter name dan price
    public kue(String name, double price){
        this.name = name;
        this.price = price;
    }
    // method getter
    public String getName() {
        return name;
    }
    // method setter
    public void setName(String name) {
        this.name = name;
    }
    // method getter
    public double getPrice() {
        return price;
    }
    // method setter
    public void setPrice(double price) {
        this.price = price;
    }
    // method abstrak yang diimplementasikan untuk menghitung harga kue, berat kue, dan jumlah kue
    abstract public double hitungHarga();
    abstract public double Berat();
    abstract public double Jumlah();
    // Anotasi untuk menandakan bahwa metode yang dituliskan 
    // akan menggantikan metode yang sama di kelas induk.
    @Override
    // method yang mengembalikan string dari objek 'kue'
    public String toString(){
        return String.format("\n---------- "+getName()+" ----------"+"\nHarga\t\t:"+getPrice());
    }
}

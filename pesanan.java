// implementasi dari class turunan 'pesanan' yang menge-extend class kue
public class pesanan extends kue{
    // variabel instance dari class 'pesanan'
    private double Berat;
    private double Jumlah;

    // konstruktor dengan parameter 'name' untuk menyimpan nama kue
    // konstruktor dengan parameter 'price' untuk menyimpan hraga kue
    // konstruktor dengan parameter 'berat' untuk menyimpan berat kue
    public pesanan(String name, double price, double berat) {
        // pemanggilan konstruktor dari induk 'kue' untuk menginisialisasi variabel name dan price
        super(name, price);
        this.Berat = berat;
    }
    // method mengembalikan nilai dari variabel berat
    public double Berat(){
        return Berat;
    }
    // method mengembalikan nilai dari variabel jumlah

    public double Jumlah(){
        return Jumlah;
    }
    // method dari class induk 'kue' untuk menghitung harga satuan berat
    // dengan berat kue yg dipesan
    public double hitungHarga() {
        return getPrice() * Berat();
    }
 
}